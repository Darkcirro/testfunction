﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunning {
    public class Obstracle : StateLookUp
    {
        GameManager gameManager;

        [SerializeField] protected float _moveSpeed = 5f;
        [SerializeField] protected bool _isBGSpeed = true;

        private void Awake()
        {
            gameManager = FindObjectOfType<GameManager>();
        }

        public void Movement()
        {
            if (gameManager == null) return;

            if (_isBGSpeed)
            {
                _moveSpeed = gameManager._gameSpeed;
            }

            transform.Translate(Vector2.left * _moveSpeed * Time.deltaTime);
        }
    }
}

