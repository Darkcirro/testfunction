﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunning {

    public class GameManager : StateLookUp
    {
        [SerializeField] PlayerController player;
        [SerializeField] GameObject background;
        Vector2 vec;

        public float _gameSpeed;
        // Start is called before the first frame update
        void Start()
        {
            state = GameState.Start;

            gameStates[GameState.Start] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        if(Camera.main.transform.position.x < player.transform.position.x)
                        {
                            var playerPos = player.transform.position;
                            var cPos = Camera.main.transform.position;
                            cPos.x = playerPos.x;
                            Camera.main.transform.position = cPos;

                            state = GameState.Play;
                        }
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };

            gameStates[GameState.Play] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        //GetGameSpeed();
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };

            gameStates[GameState.End] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };
        }

        // Update is called once per frame
        void Update()
        {
            gameStates[state](GameMethod.Update);
        }        

        void GetGameSpeed()
        {
            _gameSpeed = player.GetMoveSpeed();

            vec = background.GetComponent<Renderer>().material.GetTextureOffset("_MainTex");
            vec -= ((_gameSpeed / 40.96f) * Vector2.left * Time.deltaTime); // 40.96f is background x scale
            background.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", vec);
        }
    }

}