﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunning
{
    public class PlayerController : Player
    {
        [Header("GroundCheck")]
        [SerializeField] Transform groundCheck;
        [SerializeField] LayerMask groundLayers;

        [Header("Animator")]
        [SerializeField] Animator playerAnim;

        Rigidbody2D playerRB;

        // Start is called before the first frame update
        void Start()
        {
            playerRB = GetComponent<Rigidbody2D>();

            gameStates[GameState.Start] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        SetUpBeforePlay();
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };

            gameStates[GameState.Play] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        ChangeGravity();
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };

            gameStates[GameState.End] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };
        }

        // Update is called once per frame
        void Update()
        {
            gameStates[state](GameMethod.Update);
        }

        void FixedUpdate()
        {
            gameStates[state](GameMethod.FixedUpdate);
        }

        void SetUpBeforePlay()
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector2(0, transform.position.y), _moveSpeed * Time.deltaTime);
            playerAnim.SetBool("isMoving", true);

            if(transform.position.x == 0)
            {
                StartCoroutine(AcceleratePerSec());
                state = GameState.Play;
            }
        }

        void ChangeGravity()
        {
            if (checkGround)
            {
                if (Input.GetMouseButtonDown(0) && CheckGround())
                {
                    playerRB.gravityScale = -playerRB.gravityScale;

                    Vector3 scale = transform.localScale;
                    scale.y = -scale.y;

                    transform.localScale = scale;
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    playerRB.gravityScale = -playerRB.gravityScale;

                    Vector3 scale = transform.localScale;
                    scale.y = -scale.y;

                    transform.localScale = scale;
                }
            }

        }

        bool CheckGround()
        {           
            return Physics2D.OverlapCircle(groundCheck.position, .15f, groundLayers);
        }        
    }
}


