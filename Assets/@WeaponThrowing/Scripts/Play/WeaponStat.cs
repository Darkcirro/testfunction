﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponThrowing
{
    [CreateAssetMenu(fileName = "Data", menuName = "WeaponThrowing/WeaponData", order = 2)]
    public class WeaponStat : ScriptableObject
    {
        public GameObject weaponPrefebs;
        public Sprite weaponSprite;

        [Header("Main")]
        public int _Lv;
        public int _Damage;
        public int _Speed;
        public int _Durability;
        public int _baseCost;

        [Header("Hidden")]
        public int _lvFiveCost;
        public int _lvTenCost;
    }

}
