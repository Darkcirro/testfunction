﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponThrowing
{
    [CreateAssetMenu(fileName = "Data", menuName = "WeaponThrowing/UnitData", order = 1)]
    public class UnitBaseData : ScriptableObject
    {
        public string unitName;

        [Header("Show Stat")]
        public int _HP;
        public int _STA;
        public int _ATK;
        public int _DEF;
        public int _SPD;
        public int _LUCK;

        [Header("Hidden Stat")]
        public int _HPRegenPerSec;
        public int _STARegenPerSec;

        [Header("IncreasePerLV")]
        public int _HPPerLV;
        public int _STAPerLV;
        public int _ATKPerLV;
        public int _DEFPerLV;
        public int _SPDPerLV;
        public int _LUCKPerLV;
        public int _HPRegenPerLV;
        public int _STARegenPerLV;

    }

    [System.Serializable]
    public class CurrentUnitData
    {
        public string unitName;
        public int currentLV;
        public int currentHP;
        public int currentStamina;
        public int currentATK;
        public int currentDEF;
        public int currentSPD;
        public int currentLUCK;
        public int currentHPRegen;
        public int currentSTARegen;
    }
}

