﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponThrowing
{
    public class PlayerController : UnitController
    {
        GameController controller;

        [Header("Stat")]
        public StatBoard statBoard;
        public CurrentUnitData currentData;
        public int _currentHP;
        public int _currentSTA;
        public UnitBaseData _unitData;

        [Header("Usable")]
        public List<ItemButton> allUsable = new List<ItemButton>();


        public Transform hitPos;
        // Start is called before the first frame update
        void Start()
        {
            currentData.currentLV = 5;
            InitData(currentData, _unitData);
            InitStatBoard(statBoard, currentData);

            _currentHP = currentData.currentHP;
            _currentSTA = currentData.currentStamina;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void GenerateWeapon(ItemButton weapon)
        {
           
        }

        #region UseOverride
        //public override void InitData(CurrentUnitData currentData, UnitBaseData baseData)
        //{
        //    base.InitData(currentData, baseData);
        //}

        //public override void InitStatBoard(StatBoard board, CurrentUnitData currentData)
        //{
        //    base.InitStatBoard(board, currentData);
        //}
        #endregion
    }
}
