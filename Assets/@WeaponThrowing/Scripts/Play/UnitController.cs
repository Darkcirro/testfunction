﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponThrowing
{
    public class UnitController : MonoBehaviour
    {
        #region Virtual
        //public virtual void InitData(CurrentUnitData currentData, UnitBaseData baseData)
        //{
        //    currentData.unitName = baseData.unitName;
        //    currentData.currentHP = baseData._HP + baseData._HPPerLV * currentData.currentLV;
        //    currentData.currentStamina = baseData._STA + baseData._STAPerLV * currentData.currentLV;
        //    currentData.currentATK = baseData._ATK + baseData._ATKPerLV * currentData.currentLV;
        //    currentData.currentDEF = baseData._DEF + baseData._DEFPerLV * currentData.currentLV;
        //    currentData.currentSPD = baseData._SPD + baseData._SPDPerLV * currentData.currentLV;
        //    currentData.currentLUCK = baseData._LUCK + baseData._LUCKPerLV * currentData.currentLV;
        //    currentData.currentHPRegen = baseData._HPRegenPerSec + baseData._HPRegenPerLV * currentData.currentLV;
        //    currentData.currentSTARegen = baseData._STARegenPerSec + baseData._STARegenPerLV * currentData.currentSTARegen;
        //}

        //public virtual void InitStatBoard(StatBoard board, CurrentUnitData currentData)
        //{
        //    board.nameText.text = currentData.unitName;
        //    board.UpdateLevel(currentData.currentLV);
        //    board.UpdateName(currentData.unitName);
        //    board.UpdateHealth(currentData.currentHP, currentData.currentHP);
        //    board.UpdateStamina(currentData.currentStamina, currentData.currentStamina);
        //}
        #endregion

        public void InitData(CurrentUnitData currentData, UnitBaseData baseData)
        {
            currentData.unitName = baseData.unitName;
            currentData.currentHP = baseData._HP + baseData._HPPerLV * currentData.currentLV;
            currentData.currentStamina = baseData._STA + baseData._STAPerLV * currentData.currentLV;
            currentData.currentATK = baseData._ATK + baseData._ATKPerLV * currentData.currentLV;
            currentData.currentDEF = baseData._DEF + baseData._DEFPerLV * currentData.currentLV;
            currentData.currentSPD = baseData._SPD + baseData._SPDPerLV * currentData.currentLV;
            currentData.currentLUCK = baseData._LUCK + baseData._LUCKPerLV * currentData.currentLV;
            currentData.currentHPRegen = baseData._HPRegenPerSec + baseData._HPRegenPerLV * currentData.currentLV;
            currentData.currentSTARegen = baseData._STARegenPerSec + baseData._STARegenPerLV * currentData.currentSTARegen;
        }

        public void InitStatBoard(StatBoard board, CurrentUnitData currentData)
        {
            board.nameText.text = currentData.unitName;
            board.UpdateLevel(currentData.currentLV);
            board.UpdateName(currentData.unitName);
            board.UpdateHealth(currentData.currentHP, currentData.currentHP);
            board.UpdateStamina(currentData.currentStamina, currentData.currentStamina);
        }
    }
}

