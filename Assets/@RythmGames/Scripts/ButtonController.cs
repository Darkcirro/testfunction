﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rythm
{
    public class ButtonController : MonoBehaviour
    {
        SpriteRenderer theSR;
        [SerializeField] Sprite defaultImage;
        [SerializeField] Sprite pressedImage;

        public KeyCode keyToPress;

        // Start is called before the first frame update
        void Start()
        {
            theSR = GetComponent<SpriteRenderer>();
        }

        // Update is called once per frame
        void Update()
        {
            PressedButton();
        }

        private void PressedButton()
        {
            if (Input.GetKeyDown(keyToPress))
            {
                theSR.sprite = pressedImage;
            }

            if (Input.GetKeyUp(keyToPress))
            {
                theSR.sprite = defaultImage;
            }
        }
    }
}

