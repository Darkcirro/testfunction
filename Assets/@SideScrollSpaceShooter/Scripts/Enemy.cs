﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScrollShooting
{
    public class Enemy : Unit
    {
        [Header("Movement")]
        WaveConfig waveConfig;
        List<Transform> wayPoints = new List<Transform>();
        int waypointIndex = 0;
        int waypointPosIndex = 0;

        [SerializeField] bool targetOnPlayer = false;
               
        // Start is called before the first frame update
        void Start()
        {
            switch (waveConfig.GetWaveType())
            {
                case WaveType.Individual:
                    wayPoints = waveConfig.GetWayPoint();
                    break;
                case WaveType.Group:
                    wayPoints = waveConfig.GetTargetWayPoint(waypointPosIndex);
                    break;
                default:
                    break;
            }

            transform.position = wayPoints[waypointIndex].transform.position;

            // Random
            //nextAttack = Random.Range(attackRateRandom.x, attackRateRandom.y);
        }

        // Update is called once per frame
        void Update()
        {
            Movement();
            Shoot();
        }

        public void SetWaveConfig(WaveConfig waveConfig)
        {
            this.waveConfig = waveConfig;
        }

        void Movement()
        {
            if (waypointIndex < wayPoints.Count)
            {
                var targetPos = wayPoints[waypointIndex].transform.position;
                var movementThisFrame = waveConfig.GetMoveSpeed() * Time.deltaTime;
                transform.position = Vector2.MoveTowards(transform.position, targetPos, movementThisFrame);

                if (transform.position == targetPos)
                {
                    waypointIndex++;
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Shoot()
        {
            if(bullet == null || shootPosition == null)
            {
                Debug.LogError("@ Missing Bullet or ShootingPosition");
                return;
            }

            // Not Random
            if (Time.time >= nextAttack)
            {
                GenerateBullet(bullet, shootPosition, Vector2.left);

                nextAttack = Time.time + 1f / attackRate;
            }

            // Random
            //if (Time.time >= nextAttack)
            //{
            //    GenerateBullet(bullet, shootPosition, Vector2.left);

            //    nextAttack = Time.time + 1f / Random.Range(attackRateRandom.x, attackRateRandom.y);
            //}
        }

        public void SetWayPointPosIndex(int index)
        {
            waypointPosIndex = index;
        }

        public override void GenerateBullet(GameObject bullet, Transform pos, Vector3 dir, Transform target = null, float rotateZ = 0)
        {
            switch (bullet.GetComponent<Bullet>().type)
            {
                case Bullet.BulletType.Normal:
                    if (targetOnPlayer)
                    {
                        var y = transform.position.y - FindObjectOfType<Player>().transform.position.y;
                        var x = transform.position.x - FindObjectOfType<Player>().transform.position.x;
                        rotateZ = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
                       // Debug.Log(rotateZ);
                    }
                    break;
                case Bullet.BulletType.Homing:
                    bullet.GetComponent<Bullet>().SetTarget(FindObjectOfType<Player>().transform);
                    break;
                case Bullet.BulletType.Piece:
                    break;
                case Bullet.BulletType.Missile:
                    break;
                default:
                    break;
            }
            base.GenerateBullet(bullet, pos, dir, null, rotateZ);
        }

        public override void Die()
        {
            base.Die();
        }
    }

}

