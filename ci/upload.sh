#!/bin/bash

command -v curl > /dev/null || $(apt-get update && apt-get install curl -y)

curl -k --location --request POST https://172.30.108.42:4433/apps/${CI_PROJECT_PATH}/upload  --form file=@Builds/${BUILD_TARGET}/${BUILD_NAME}.apk --form tag=${BUILD_TARGET} --form version=${CI_COMMIT_TAG}
