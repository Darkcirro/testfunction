﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rythm
{
    public class BeatScroller : MonoBehaviour
    {
        [SerializeField] float beatTempo;

        public bool hasStarted;

        // Start is called before the first frame update
        void Start()
        {
            beatTempo = beatTempo / 60f;
        }

        // Update is called once per frame
        void Update()
        {
            CheckStartGame();
        }

        private void CheckStartGame()
        {
            if (!hasStarted)
            {
                
            }
            else
            {
                transform.position -= new Vector3(0f, beatTempo * Time.deltaTime, 0f);
            }
        }


    }
}


