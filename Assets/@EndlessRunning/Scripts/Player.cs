﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunning {
    public class Player : StateLookUp
    {
        [Header("Movement")]
        [SerializeField] protected PlayerData _data;
        protected float _moveSpeed;
        protected float _accelerate;
        protected float _maxSpeed;

        [Header("Check Ground")]
        protected bool checkGround = false;

        private void Awake()
        {
            InitData();
        }

        void InitData()
        {
            _moveSpeed = _data._moveSpeed;
            _accelerate = _data._accelerate;
            _maxSpeed = _data._maxSpeed;

            if (_data.checkGround) checkGround = true;
            else checkGround = false;
        }

        public float GetMoveSpeed()
        {
            return _moveSpeed;
        }

        public IEnumerator AcceleratePerSec()
        {
            while (true)
            {
                yield return new WaitForSeconds(1f);
                _moveSpeed += _accelerate;
            }
        }
    }
}

