﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponThrowing
{
    public class EnemyController : UnitController
    {
        GameController controller;

        [Header("Stat")]
        public StatBoard statBoard;
        public CurrentUnitData currentData;
        public int _currentHP;
        public int _currentSTA;
        public UnitBaseData _unitData;

        public Transform hitPos;

        // Start is called before the first frame update
        void Start()
        {
            currentData.currentLV = Random.Range(1,6);
            InitData(currentData, _unitData);
            InitStatBoard(statBoard, currentData);

            _currentHP = currentData.currentHP;
            _currentSTA = currentData.currentStamina;
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        #region UseOverride
        //public override void InitData(CurrentUnitData currentData, UnitBaseData baseData)
        //{
        //    base.InitData(currentData, baseData);
        //}

        //public override void InitStatBoard(StatBoard board, CurrentUnitData currentData)
        //{
        //    base.InitStatBoard(board, currentData);
        //}
        #endregion
    }
}


