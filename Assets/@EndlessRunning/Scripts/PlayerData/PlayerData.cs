﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunning {

    [CreateAssetMenu(fileName = "Data", menuName = "EndlessRunning/PlayerData", order = 1)]
    public class PlayerData : ScriptableObject
    {
        [Header("Movement")]
        public float _moveSpeed = 3f;
        public float _accelerate = .25f;
        public float _maxSpeed = 5f;

        [Header("Check Ground")]
        public bool checkGround = false;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
