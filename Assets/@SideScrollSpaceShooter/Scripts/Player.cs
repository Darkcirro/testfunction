﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScrollShooting
{
    public class Player : Unit
    {
        Vector2 movement;

        [Header("Movement")]
        [SerializeField] float _moveSpeed = 350;
        [SerializeField] float _boundDistance = .25f;
              
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            Movement();
            Shoot();
        }

        void Movement()
        {
            movement.x = Input.GetAxisRaw("Horizontal") * Time.deltaTime;
            movement.y = Input.GetAxisRaw("Vertical") * Time.deltaTime;

            transform.Translate(movement * _moveSpeed);

            var ver = Camera.main.orthographicSize;
            var hor = ver * Screen.width / Screen.height;

            var posX = Mathf.Clamp(transform.position.x, -hor + _boundDistance, hor - _boundDistance);
            var posY = Mathf.Clamp(transform.position.y, -ver + _boundDistance, ver - _boundDistance);

            transform.position = new Vector2(posX, posY);
        }

        void Shoot()
        {
            if (Time.time >= nextAttack)
            {
                if (Input.GetMouseButton(0))
                {
                    GenerateBullet(bullet, shootPosition, Vector2.right);
                
                    nextAttack = Time.time + 1f / attackRate;
                }
            }            
        }

        public override void GenerateBullet(GameObject bullet, Transform pos, Vector3 dir, Transform target = null, float rotateZ = 0)
        {
            if (bullet.GetComponent<Bullet>().type == Bullet.BulletType.Homing)
            {
                target = GetNearestEnemy();

                if (target != null)
                {
                    Debug.Log(target);
                    base.GenerateBullet(bullet, pos, dir, target);
                }
            }
            else
            {
                base.GenerateBullet(bullet, pos, dir);
            }
        }

        private Transform GetNearestEnemy()
        {
            Transform targetEnemy = null;
            float dis = 9999;
            foreach (var enemy in FindObjectsOfType<Enemy>())
            {
                if(Vector3.Distance(transform.position, enemy.transform.position) < dis)
                {
                    dis = Vector3.Distance(transform.position, enemy.transform.position);
                    targetEnemy = enemy.transform;
                }
            }

            return targetEnemy;
        }

        public override void Die()
        {
            base.Die();
        }

    }

}

