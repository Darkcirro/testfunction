﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rythm
{
    public class NoteObject : MonoBehaviour
    {
        [SerializeField] bool canBePressed = false;

        [SerializeField] KeyCode keyToPress;

        bool hit = false;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            PressButton();
        }

        private void PressButton()
        {
            if (Input.GetKeyDown(keyToPress))
            {
                if (canBePressed)
                {
                    hit = true;
                    gameObject.SetActive(false);
                    
                    //GameManager.instance.NoteHit();

                    if(Mathf.Abs(transform.position.y) > 0.25f)
                    {                       
                        GameManager.instance.NormalHit();
                        EffectHandle.current.PlaySelectEffect("Hit", transform.position);
                    }
                    else if (Mathf.Abs(transform.position.y) > 0.05f)
                    {
                        GameManager.instance.GoodHit();
                        EffectHandle.current.PlaySelectEffect("Good", transform.position);
                    }
                    else
                    {
                        GameManager.instance.PerfectHit();
                        EffectHandle.current.PlaySelectEffect("Perfect", transform.position);
                    }
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag.Equals("Activator"))
            {
                canBePressed = true;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag.Equals("Activator") && !hit)
            {
                canBePressed = false;
                GameManager.instance.NoteMiss();
                EffectHandle.current.PlaySelectEffect("Miss", transform.position);
            }
        }
    }
}

