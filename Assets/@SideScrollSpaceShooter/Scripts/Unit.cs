﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScrollShooting
{
    public class Unit : MonoBehaviour
    {
        [Header("Shooting")]
        [SerializeField] protected Transform shootPosition;
        [SerializeField] protected GameObject bullet;

        [SerializeField] protected Vector2 attackRateRandom = Vector2.zero;
        [SerializeField] protected float attackRate = 2f;
        protected float nextAttack = 0;

        public virtual void GenerateBullet(GameObject bullet, Transform pos, Vector3 dir, Transform target = null, float rotateZ = 0)
        {
            GameObject bulletTemp = Instantiate(bullet, pos.position, transform.rotation * Quaternion.Euler(0,0,rotateZ));
            bulletTemp.GetComponent<Bullet>().dir = dir;

            if(bulletTemp.GetComponent<Bullet>().type == Bullet.BulletType.Homing)
            {
                if(target != null)
                {
                    bulletTemp.GetComponent<Bullet>().SetTarget(target);
                }
            }

        }

        public virtual void Die()
        {
            Destroy(gameObject);
        }
    }

}

