﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Rythm
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        [SerializeField] bool startPlaying;
        [SerializeField] AudioSource music;
        [SerializeField] BeatScroller theBS;

        [Header("Score")]
        [SerializeField] Text scoreText;
        [SerializeField] Text multiText;

        [SerializeField] int currentScore = 0;
        [SerializeField] int scorePerNote = 50;
        [SerializeField] int scorePerGoodNote = 100;
        [SerializeField] int scorePerPerfectNote = 300;

        [SerializeField] int currentCombo = 0;

        [SerializeField] int currentMultiplier = 1;
        [SerializeField] int multiplierTracker = 0;
        [SerializeField] int[] multiplierThresholds;

        // Start is called before the first frame update
        void Start()
        {
            instance = this;

            scoreText.text = "Score : " + currentScore;
            multiText.text = "Combo : x" + currentCombo;

            currentMultiplier = 1;
        }

        // Update is called once per frame
        void Update()
        {
            if (!startPlaying)
            {
                if (Input.anyKeyDown)
                {
                    startPlaying = true;
                    theBS.hasStarted = true;

                    music.Play();
                }
            }
        }

        public void NoteHit()
        {
            Debug.Log("Hit on time!");

            if(currentMultiplier-1 < multiplierThresholds.Length)
            {
                multiplierTracker++;

                if (multiplierThresholds[currentMultiplier - 1] <= multiplierTracker)
                {
                    multiplierTracker = 0;
                    currentMultiplier++;
                }
            }

            currentCombo++;

            //currentScore += scorePerNote * currentMultiplier;

            scoreText.text = "Score : " + currentScore;
            multiText.text = "Combo : x" + currentCombo; 
        }

        public void NormalHit()
        {
            Debug.Log("Normal!");
            currentScore += scorePerNote * currentMultiplier;
            NoteHit();            
        }

        public void GoodHit()
        {
            Debug.Log("Good!");
            currentScore += scorePerGoodNote * currentMultiplier;
            NoteHit();
        }

        public void PerfectHit()
        {
            Debug.Log("Perfect!");
            currentScore += scorePerPerfectNote * currentMultiplier;
            NoteHit();
        }

        public void NoteMiss()
        {
            Debug.Log("Miss!");
            //currentMultiplier = 0;

            currentMultiplier = 1;
            multiplierTracker = 0;

            currentCombo = 0;

            multiText.text = "Combo : x" + currentCombo;
        }
    }
}

