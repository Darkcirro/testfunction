#!/usr/bin/env bash

set -e
set -x

${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -quit \
  -batchmode \
  -returnlicense
