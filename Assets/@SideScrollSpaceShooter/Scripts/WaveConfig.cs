﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScrollShooting
{
    [CreateAssetMenu(fileName = "NewWaveConfig", menuName = "SideScrollShooting/Enemy Wave Config", order = 2)]
    public class WaveConfig : ScriptableObject
    {
        [SerializeField] WaveType waveType;

        [SerializeField] GameObject enemyPrefeb;
        [SerializeField] GameObject pathPrefeb;
        [SerializeField] float spawnRandomFactor = .3f;
        [SerializeField] float moveSpeed = 2f;
        [SerializeField] int numberOfEnemies = 5;
        [SerializeField] float timeBetweenSpawns = .5f;

        public GameObject GetEnemyPrefeb() { return enemyPrefeb; }

        public List<Transform> GetWayPoint() {

            var waveWayPoints = new List<Transform>();

            foreach (Transform child in pathPrefeb.transform)
            {
                waveWayPoints.Add(child);
            }

            return waveWayPoints;            
        }

        public List<Transform> GetTargetWayPoint(int i)
        {
            if(i >= pathPrefeb.transform.childCount)
            {
                Debug.LogError("@ Out of Range");
                return null;
            }

            var waveWayPoints = new List<Transform>();

            foreach (Transform child in pathPrefeb.transform.GetChild(i).transform)
            {
                waveWayPoints.Add(child);
            }

            return waveWayPoints;
        }

        public int GetEnemyGroupCount()
        {
            return pathPrefeb.transform.childCount;
        }

        public float GetTimeBetweenSpawns() { return timeBetweenSpawns; }

        public float GetSpawnRandomFactor() { return spawnRandomFactor; }

        public int GetNumberOfEnemies() { return numberOfEnemies; }

        public float GetMoveSpeed() { return moveSpeed; }


        public WaveType GetWaveType() { return waveType; }


        [System.Serializable]
        public class WaveGroupWayPoint
        {
            public List<Transform> wayPoints = new List<Transform>();
        }
    }

    public enum WaveType
    {
        Individual, Group
    }
}
