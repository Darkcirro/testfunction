#!/usr/bin/env bash

set -e
set -x
mkdir -p /root/.cache/unity3d
mkdir -p /root/.local/share/unity3d/Unity/
set +x

UPPERCASE_BUILD_TARGET=${BUILD_TARGET^^};

if [ $UPPERCASE_BUILD_TARGET = "ANDROID" ]
then
    if [ -n $ANDROID_KEYSTORE_BASE64 ]
	then
        echo '$ANDROID_KEYSTORE_BASE64 found, decoding content into keystore.keystore'
        echo $ANDROID_KEYSTORE_BASE64 | base64 --decode > keystore.keystore
    else
        echo '$ANDROID_KEYSTORE_BASE64'" env var not found, building with Unity's default debug keystore"
    fi
fi

LICENSE="UNITY_LICENSE_CONTENT_"$UPPERCASE_BUILD_TARGET

# if [ -z "${!LICENSE}" ]
# then
#     echo "$LICENSE env var not found, using default UNITY_LICENSE_CONTENT env var"
#     LICENSE=UNITY_LICENSE_CONTENT
# else
#     echo "Using $LICENSE env var"
# fi
echo "UNITY_VERSION "$UNITY_VERSION
if [ $UNITY_VERSION = "2019.4.10f1" ]
then
    LICENSE=UNITY_LICENSE_CONTENT_2019_4_10f1
    echo "Writing UNITY_LICENSE_CONTENT_2019_4_10f1 to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf"
elif [ $UNITY_VERSION = "2019.2.21f1" ]
then
	if [ $UPPERCASE_BUILD_TARGET = "ANDROID" ]
	then
		LICENSE=UNITY_LICENSE_CONTENT_2019_2_21f1_ANDROID
		echo "${!LICENSE}" | tr -d '\r' > /headless/.local/share/unity3d/Unity/Unity_lic.ulf
		echo "Writing UNITY_LICENSE_CONTENT_2019_2_21f1_ANDROID to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf"
	else
		LICENSE=UNITY_LICENSE_CONTENT_2019_2_21f1
		echo "Writing UNITY_LICENSE_CONTENT_2019_2_21f1 to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf"
    fi
elif [ $UNITY_VERSION = "5.6.7" ]
then
    LICENSE=UNITY_LICENSE_CONTENT_5_6_7
    echo "Writing UNITY_LICENSE_CONTENT_5_6_7 to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf"
else
    LICENSE=UNITY_LICENSE_CONTENT
    echo "Writing UNITY_LICENSE_CONTENT to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf"
fi

echo "${!LICENSE}" | tr -d '\r' > /root/.local/share/unity3d/Unity/Unity_lic.ulf

set -x
