﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScrollShooting
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] List<WaveConfig> waveConfig = new List<WaveConfig>();
        [SerializeField] int startingWave = 0;
        [SerializeField] bool looping = false;
        [SerializeField] float timeBetweenWave = .5f;

        // Start is called before the first frame update
        IEnumerator Start()
        {
            do
            {
                yield return StartCoroutine(SpawnAllWave());
            } while (looping);
        }

        IEnumerator SpawnAllWave()
        {
            for (int i = startingWave; i < waveConfig.Count; i++)
            {
                var currentWave = waveConfig[i];
                switch (waveConfig[i].GetWaveType())
                {
                    case WaveType.Individual:
                        yield return StartCoroutine(SpawnAllEnemiesInWave(currentWave));
                        break;
                    case WaveType.Group:
                        yield return StartCoroutine(SpawnAllEnemiesInLine(currentWave));
                        break;
                    default:
                        break;
                }
            }
        }

        IEnumerator SpawnAllEnemiesInWave(WaveConfig waveConfig)
        {
            for (int i = 0; i < waveConfig.GetNumberOfEnemies(); i++)
            {
                var newEnemy = Instantiate(waveConfig.GetEnemyPrefeb(), waveConfig.GetWayPoint()[0].transform.position, Quaternion.identity);
                newEnemy.GetComponent<Enemy>().SetWaveConfig(waveConfig);
                yield return new WaitForSeconds(waveConfig.GetTimeBetweenSpawns());
            }

            yield return new WaitForSeconds(timeBetweenWave);
        }

        IEnumerator SpawnAllEnemiesInLine(WaveConfig waveConfig)
        {
            for (int i = 0; i < waveConfig.GetEnemyGroupCount(); i++)
            {
                var newEnemy = Instantiate(waveConfig.GetEnemyPrefeb(), waveConfig.GetTargetWayPoint(i)[0].transform.position, Quaternion.identity);
                newEnemy.GetComponent<Enemy>().SetWaveConfig(waveConfig);
                newEnemy.GetComponent<Enemy>().SetWayPointPosIndex(i);
            }

            yield return new WaitForSeconds(timeBetweenWave);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}