﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TPSGame
{
    public class PlayerMovement : MonoBehaviour
    {
        public CharacterController controller;
        public Transform cam;
        public float _speed = 12f;
        public float gravity = -9.81f;

        public float turnSmoothTime = .1f;
        float turnSmoothVelocity;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Movement();
        }

        void Movement()
        {
            Cursor.lockState = CursorLockMode.Locked;

            float x = Input.GetAxisRaw("Horizontal");
            float z = Input.GetAxisRaw("Vertical");
            Vector3 direction = new Vector3(x, 0, z).normalized;

            if(direction.magnitude >= .1f){
                float targetAngle = Mathf.Atan2(direction.x,direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0);

                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                controller.Move(moveDir * _speed * Time.deltaTime);
            }
            
        }
    }
}


