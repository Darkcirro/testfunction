﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MeleeCombat {

    public class PlayerController : MonoBehaviour
    {
        Rigidbody2D playerRB;
        Animator anim;

        Vector2 movement;

        [Header("Status")]
        [SerializeField] float _HP = 100;
        [SerializeField] float currentHP;
        [SerializeField] float _moveSpeed = 150;
        [SerializeField] float attackDamage = 20.0f;

        [Header("Attack")]
        [SerializeField] Transform attackPoint;
        [SerializeField] float attackRange = .5f;
        [SerializeField] LayerMask enemyLayer;

        [SerializeField] float attackRate = 2f;
        float nextAttack = 0;
        public bool isAttack = false;
        // Start is called before the first frame update
        void Start()
        {
            playerRB = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();
            currentHP = _HP;
        }

        // Update is called once per frame
        void Update()
        {
            Attack();
        }

        private void FixedUpdate()
        {
            Movement();
        }

        void Movement()
        {
            movement.x = Input.GetAxisRaw("Horizontal");
            
            if(movement.x > 0)
            {
                transform.localScale = new Vector3(-1, 1, 1);
            }
            else if(movement.x < 0)
            {
                transform.localScale = new Vector3(1, 1, 1);
            }

            anim.SetFloat("moveX", Mathf.Abs(movement.x));

            if (!isAttack)
                playerRB.velocity = new Vector2(movement.x * _moveSpeed * Time.deltaTime, playerRB.velocity.y);
            else
                playerRB.velocity = Vector3.zero;
        }

        void Attack()
        {            
            if(Time.time >= nextAttack)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    isAttack = true;
                    anim.SetTrigger("Attack");
                                       

                    nextAttack = Time.time + 1f / attackRate;
                }                
            }
        }

        //Use in Animation
        void CheckAttack()
        {
            Collider2D[] hit = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayer);

            if (hit != null)
            {
                foreach (var item in hit)
                {
                    item.GetComponent<Enemy>().GetDamage(attackDamage);
                }
            }
        }

        //Use in Animation
        void FinishAttack()
        {
            isAttack = false;
        }

        public void GetDamage(float value)
        {
            currentHP -= value;

            anim.SetTrigger("hurt");

            if (currentHP <= 0)
            {
                Die();
            }
        }

        void Die()
        {
            anim.SetBool("isDeath", true);

            GetComponent<Collider2D>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            playerRB.velocity = Vector2.zero;
            this.enabled = false;
        }

        private void OnDrawGizmosSelected()
        {
            if (attackPoint == null) return;
            Gizmos.DrawWireSphere(attackPoint.position, attackRange);
        }
    }

}
