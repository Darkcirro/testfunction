﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace SceneLoader
{
    public class SceneLoaderController : MonoBehaviour
    {
        [SerializeField] Animator anim;
        [SerializeField] Image fadedImage;
        [SerializeField] bool fadedIn = false;

        [SerializeField] float timeBeforeFaded = 0f;

        string sceneName;
        // Start is called before the first frame update
        void Start()
        {
            if (fadedIn)
            {
                fadedImage.color = new Color(0, 0, 0, 255);
                anim.SetTrigger("FadeIn");
            }
            else
            {
                fadedImage.color = new Color(0, 0, 0, 0);
                fadedImage.gameObject.SetActive(false);
            }
        }

        public void LoadTargetScene(string targetScene)
        {
            SceneManager.LoadScene(targetScene);
        }

        #region UseFadedOut
        public void LoadTargetSceneFaded(string target)
        {
            StartCoroutine(WaitBeforeFaded(target));
        }

        IEnumerator WaitBeforeFaded(string targetName)
        {
            yield return new WaitForSeconds(timeBeforeFaded);
            anim.SetTrigger("FadeOut");
            sceneName = targetName;
        }

        // use in Animation
        void LoadSceneInAnimation()
        {
            SceneManager.LoadScene(sceneName);
        }
        #endregion
    }
}

