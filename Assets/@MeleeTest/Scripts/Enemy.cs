﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MeleeCombat
{
    public class Enemy : MonoBehaviour
    {
        Animator enemyAnim;
        Rigidbody2D rb;

        [SerializeField] float maxHealth = 100.0f;
        [SerializeField] float currentHealth;
        [SerializeField] float attackDamage;

        [SerializeField] Transform attackPoint;
        [SerializeField] float attackRange = .5f;
        [SerializeField] LayerMask playerLayer;

        [SerializeField] float attackRate = 2f;
        float nextAttack = 0;
        public bool isAttack = false;
        // Start is called before the first frame update
        void Start()
        {
            currentHealth = maxHealth;
            enemyAnim = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            CheckPlayer();
            Movement();
        }

        public void GetDamage(float value)
        {
            currentHealth -= value;

            enemyAnim.SetTrigger("hurt");

            if(currentHealth <= 0)
            {
                Die();
            }
        }

        void Movement()
        {
            var player = FindObjectOfType<PlayerController>();
            if(player.transform.position.x - transform.position.x < 0)
            {
                transform.localScale = new Vector3(1, 1, 1);
            }else if (player.transform.position.x - transform.position.x > 0)
                transform.localScale = new Vector3(-1, 1, 1);
        }

        void Die()
        {
            enemyAnim.SetBool("isDeath", true);

            GetComponent<Collider2D>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            rb.velocity = Vector2.zero;
            this.enabled = false;
        }

        void CheckPlayer()
        {
            if (Time.time >= nextAttack)
            {
                Collider2D[] hit = Physics2D.OverlapCircleAll(attackPoint.position, 1f, playerLayer);


                if (hit.Length > 0)
                {
                    isAttack = true;
                    enemyAnim.SetTrigger("Attack");
                    nextAttack = Time.time + 1f / attackRate;
                }
            }

        }

        //Use in Animation
        void CheckAttack()
        {
            Collider2D[] hit = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);

            if (hit.Length > 0)
            {
                foreach (var item in hit)
                {
                    item.GetComponent<PlayerController>().isAttack = true;
                    item.GetComponent<PlayerController>().GetDamage(attackDamage);
                }
            }
        }

        //Use in Animation
        void FinishAttack()
        {
            isAttack = false;
        }

        private void OnDrawGizmosSelected()
        {
            if (attackPoint == null) return;
            Gizmos.DrawWireSphere(attackPoint.position, attackRange);
        }
    }

}