﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunning {
    public class Wall : Obstracle
    {
        // Start is called before the first frame update
        void Start()
        {
            gameStates[GameState.Start] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };

            gameStates[GameState.Play] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        Movement();
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };

            gameStates[GameState.End] = (GameMethod fc) =>
            {
                switch (fc)
                {
                    case GameMethod.Update:
                        break;
                    case GameMethod.FixedUpdate:
                        break;
                    default:
                        break;
                }
            };
        }

        // Update is called once per frame
        void Update()
        {
            gameStates[state](GameMethod.Update);
        }
    }
}


