﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FPSGame
{
    public class PlayerMovement : MonoBehaviour
    {
        public CharacterController controller;

        public float _speed = 12f;
        public float gravity = -9.81f;
        public float jumpHeight;

        public Transform groundCheck;
        public float groundDistance = .4f;
        public LayerMask groundMask;

        Vector3 velocity;
        bool isGround;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            Movement();
        }

        void Movement()
        {
            isGround = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGround && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 move = transform.right * x + transform.forward * z;

            controller.Move(move * _speed * Time.deltaTime);

            if (Input.GetButtonDown("Jump") && isGround)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }

            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);
        }
    }
}


