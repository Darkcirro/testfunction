﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace EndlessRunning
{
    public class StateLookUp : MonoBehaviour
    {
        public enum GameState
        {
            Start, Play, End
        }

        public enum GameMethod
        {
            Update, FixedUpdate
        }

        public static GameState state;
        public Dictionary<GameState, Action<GameMethod>> gameStates = new Dictionary<GameState, Action<GameMethod>>();
    }

}
