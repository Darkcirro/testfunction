﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatBoard : MonoBehaviour
{
    public Text levelText;
    public Text nameText;
    public Text healthText;
    public Image healthImg;
    public Text staminaText;
    public Image staminaImg;

    public void UpdateLevel(int lv)
    {
        levelText.text = "LV : " + lv;
    }

    public void UpdateName(string nameTxt)
    {
        nameText.text = nameTxt;
    }

    public void UpdateHealth(int currentHealth, int maxHealth)
    {
        healthText.text = "HP : " + currentHealth + "/" + maxHealth;
        healthImg.fillAmount = (float)currentHealth / (float)maxHealth;
    }

    public void UpdateStamina(int currentStamina, int maxStamina)
    {
        staminaText.text = "Stamina : " + currentStamina + "/" + maxStamina;
        staminaImg.fillAmount = (float)currentStamina / (float)maxStamina;
    }
}
