﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScrollShooting
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Bullet : MonoBehaviour
    {
        public BulletType type;
        public Vector3 dir;
        [SerializeField] float _speed;        

        bool isFinishStart = false;
        private Rigidbody2D rb;

        [Header("Missile")]
        [SerializeField] float timeBeforeLaunch = .25f;

        [Header("Homing Missile")]
        public float rotateSpeed = 200f;
        private Transform target;
        private bool isNearTarget = false;
        // Start is called before the first frame update
        void Start()
        {
            rb = GetComponent<Rigidbody2D>();

            switch (type)
            {
                case BulletType.Homing:
                    break;
                case BulletType.Missile:
                    StartCoroutine(BeforeLaunch(timeBeforeLaunch));
                    break;
                default:
                    break;
            }
        }

        IEnumerator BeforeLaunch(float maxTime)
        {
            float currentTime = 0;
            while (currentTime < maxTime)
            {
                currentTime += Time.deltaTime;
                transform.Translate(Vector2.down * Time.deltaTime);
                yield return null;
            }

            isFinishStart = true;
        }

        // Update is called once per frame
        void Update()
        {
            Movement();
        }

        private void FixedUpdate()
        {
            if (type != BulletType.Homing || isNearTarget) { return; }
            else
            {
                if (target == null)
                {
                    isNearTarget = true;
                    return;
                }
                HomingMissileMovement(dir);
            }
        }

        void Movement()
        {
            switch (type)
            {
                case BulletType.Normal:
                    transform.Translate(dir * _speed * Time.deltaTime);
                    break;
                case BulletType.Homing:
                    if (isNearTarget)
                    {
                        rb.freezeRotation = true;
                        transform.Translate(dir * _speed * Time.deltaTime);
                    }
                    break;
                case BulletType.Piece:
                    break;
                case BulletType.Missile:
                    if (isFinishStart)
                        transform.Translate(dir * _speed * Time.deltaTime);
                    break;
                default:
                    break;
            }
        }

        void HomingMissileMovement(Vector3 dir)
        {
            Vector3 movementDir = transform.right;

            if(dir == Vector3.left)
            {
                movementDir = -transform.right;
            }
            else if(dir == Vector3.right)
            {
                movementDir = transform.right;
            }

            Vector2 direction = (Vector2)target.position - rb.position;

            direction.Normalize();

            float rotateAmount = Vector3.Cross(direction, movementDir).z;

            rb.angularVelocity = -rotateAmount * rotateSpeed;

            rb.velocity = movementDir * _speed;

            if(Vector3.Distance(transform.position, target.position) <= 2f)
            {
                isNearTarget = true;
            }
        }

        public void SetTarget(Transform unit)
        {
            target = unit;
        }

        public void SetBulletSpeed(float value)
        {
            _speed = value;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag.Equals("DestroyBullet"))
            {
                Destroy(gameObject);
            }
            else if (collision.tag.Equals("Enemy"))
            {
                switch (type)
                {
                    case BulletType.Normal:
                        Destroy(gameObject);
                        break;
                    case BulletType.Homing:
                        Destroy(gameObject);
                        break;
                    case BulletType.Piece:
                        break;
                    case BulletType.Missile:
                        Destroy(gameObject);
                        break;
                    default:
                        break;
                }
            }
        }

        public enum BulletType
        {
            Normal, Homing, Piece, Missile
        }
    }
}



