﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WeaponThrowing
{
    public class GameController : MonoBehaviour
    {
        [Header("Player")]
        public GameObject playerPredestal;
        public PlayerController currentPlayer;

        [Header("Enemy")]
        public GameObject enemyPredestal;
        public EnemyController currentEnemy;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

