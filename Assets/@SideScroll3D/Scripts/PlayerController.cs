﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SideScroll3D
{
    public class PlayerController : MonoBehaviour
    {
        public CharacterController controller;

        [SerializeField] float _speed = 12f;
        [SerializeField] float gravity = -9.81f;
        [SerializeField] float jumpHeight;
        [SerializeField] int maxJump = 2;
        int jumpCount = 0;

        [Header("GroundCheck")]
        [SerializeField] Transform groundCheck;
        [SerializeField] float groundDistance = .4f;
        [SerializeField] LayerMask groundMask;

        Vector3 velocity;
        bool isGround;

        Vector3 cameraStartPos;

        [SerializeField] Transform minCamera = null;
        [SerializeField] Transform maxCamera = null;

        // Start is called before the first frame update
        void Start()
        {
            cameraStartPos = Camera.main.transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            Movement();
        }

        void Movement()
        {
            isGround = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGround && velocity.y < 0)
            {
                velocity.y = -2f;
                jumpCount = 0;
            }

            float x = Input.GetAxis("Horizontal");
            //float z = Input.GetAxis("Vertical");

            Vector3 move = transform.right * x/* + transform.forward * z*/;

            controller.Move(move * _speed * Time.deltaTime);

            if (Input.GetButtonDown("Jump") && (isGround || jumpCount < maxJump))
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
                jumpCount++;
            }

            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);

            FollowCamera();
        }

        void FollowCamera()
        {
            Camera.main.transform.position = new Vector3(Mathf.Clamp( transform.position.x + cameraStartPos.x, minCamera.transform.position.x, maxCamera.transform.position.x)
                , Mathf.Clamp( transform.position.y + cameraStartPos.y, minCamera.transform.position.y, maxCamera.transform.position.y), Camera.main.transform.position.z);            
        }
    }

}
