﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridWalk
{
    public class PlayerController : MonoBehaviour
    {
        Vector2 move;
        Vector3 dir;

        public float _moveSpeed;

        public List<PlayerController> allLineUnit = new List<PlayerController>();

        public Vector3 startPoint;
        public Vector3 endPoint;

        public bool mainPlayer = false;

        // Start is called before the first frame update
        void Start()
        {
            startPoint = transform.position;
            endPoint = transform.position + Vector3.up;

            //GameObject firstFollower = Instantiate(follower.gameObject, transform.position, Quaternion.identity);
            //firstFollower.transform.position = transform.position - Vector3.up;
            //firstFollower.GetComponent<Follower>().following = gameObject;

        }

        // Update is called once per frame
        void Update()
        {
            Movement();
        }

        private void FixedUpdate()
        {

        }

        void Movement()
        {
            if (mainPlayer)
            {
                if (Input.GetKeyDown(KeyCode.D))
                {
                    dir = Vector2.right;
                }
                else if (Input.GetKeyDown(KeyCode.A))
                {
                    dir = Vector2.left;
                }
                else if (Input.GetKeyDown(KeyCode.W))
                {
                    dir = Vector2.up;
                }
                else if (Input.GetKeyDown(KeyCode.S))
                {
                    dir = Vector2.down;
                }

                if (Vector2.Distance(transform.position, endPoint) == 0)
                {
                    for (int i = allLineUnit.Count - 1; i > 0; i--)
                    {
                        allLineUnit[i].startPoint = allLineUnit[i - 1].startPoint;
                        allLineUnit[i].endPoint = allLineUnit[i - 1].endPoint;
                    }

                    startPoint = endPoint;
                    endPoint = transform.position + dir;

                }
                transform.position = Vector3.MoveTowards(transform.position, endPoint, _moveSpeed * Time.deltaTime);
            }
            else
            {
                _moveSpeed = 5;
                transform.position = Vector3.MoveTowards(transform.position, endPoint, _moveSpeed * Time.deltaTime);
            }
        }

    }
}

