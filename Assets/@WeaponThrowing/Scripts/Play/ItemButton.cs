﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace WeaponThrowing
{
    public class ItemButton : MonoBehaviour, IPointerDownHandler
    {
        public WeaponStat weaponStat;
        public Image weaponImg;
        public Text costText;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OnPointerDown(PointerEventData eventData)
        {

        }

    }
}

